// subjects
let subjectCredits = {
  javascriptCredits: 4,
  reactCredits: 7,
  pythonCredits: 6,
  javaCredits: 3,
};

let creditSum =
  subjectCredits.javascriptCredits +
  subjectCredits.reactCredits +
  subjectCredits.pythonCredits +
  subjectCredits.javaCredits;

// students
let students = [];

// student 1
students[0] = {
  name: "Jean",
  surname: "Reno",
  age: 26,
  scores: {
    javascript: 62,
    react: 57,
    python: 88,
    java: 90,
  },
};
// sum
let jeanRenoSum =
  students[0].scores.javascript +
  students[0].scores.react +
  students[0].scores.python +
  students[0].scores.java;
students[0].scores.sum = jeanRenoSum;
// arithmetical average
let jeanRenoArthAve = jeanRenoSum / 4;
students[0].scores.arithmeticalAverage = jeanRenoArthAve;
// GPA
let jeanRenoGPA =
  (1 * subjectCredits.javascriptCredits +
    0.5 * subjectCredits.reactCredits +
    3 * subjectCredits.pythonCredits +
    3 * subjectCredits.javaCredits) /
  creditSum;
students[0].scores.GPA = jeanRenoGPA;

// student 2
students[1] = {
  name: "Claude",
  surname: "Monet",
  age: 19,
  scores: {
    javascript: 77,
    react: 52,
    python: 92,
    java: 67,
  },
};
// sum
let claudeMonetSum =
  students[1].scores.javascript +
  students[1].scores.react +
  students[1].scores.python +
  students[1].scores.java;
students[1].scores.sum = claudeMonetSum;
// arithmetical average
let claudeMonetArthAve = claudeMonetSum / 4;
students[1].scores.arithmeticalAverage = claudeMonetArthAve;
// GPA
let claudeMonetGPA =
  (2 * subjectCredits.javascriptCredits +
    0.5 * subjectCredits.reactCredits +
    4 * subjectCredits.pythonCredits +
    1 * subjectCredits.javaCredits) /
  creditSum;
students[1].scores.GPA = claudeMonetGPA;

// student 3
students[2] = {
  name: "Van",
  surname: "Gogh",
  age: 21,
  scores: {
    javascript: 51,
    react: 98,
    python: 65,
    java: 70,
  },
};
// sum
let vanGoghSum =
  students[2].scores.javascript +
  students[2].scores.react +
  students[2].scores.python +
  students[2].scores.java;
students[2].scores.sum = vanGoghSum;
// arithmetical average
let vanGoghArthAve = vanGoghSum / 4;
students[2].scores.arithmeticalAverage = vanGoghArthAve;
// GPA
let vanGoghGPA =
  (0.5 * subjectCredits.javascriptCredits +
    4 * subjectCredits.reactCredits +
    1 * subjectCredits.pythonCredits +
    1 * subjectCredits.javaCredits) /
  creditSum;
students[2].scores.GPA = vanGoghGPA;

// student 4
students[3] = {
  name: "Dam",
  surname: "Square",
  age: 36,
  scores: {
    javascript: 82,
    react: 53,
    python: 80,
    java: 65,
  },
};
// sum
let damSquareSum =
  students[3].scores.javascript +
  students[3].scores.react +
  students[3].scores.python +
  students[3].scores.java;
students[3].scores.sum = damSquareSum;
// arithmetical average
let damSquareArthAve = damSquareSum / 4;
students[3].scores.arithmeticalAverage = damSquareArthAve;
// GPA
let damSquareGPA =
  (3 * subjectCredits.javascriptCredits +
    0.5 * subjectCredits.reactCredits +
    2 * subjectCredits.pythonCredits +
    1 * subjectCredits.javaCredits) /
  creditSum;
students[3].scores.GPA = damSquareGPA;

// arithmetical averages
let generalArthAve =
  (jeanRenoArthAve + claudeMonetArthAve + vanGoghArthAve + damSquareArthAve) /
  4;

jeanRenoArthAve > generalArthAve
  ? console.log(
      `${students[0].name} ${students[0].surname} got the Red Diploma!`
    )
  : console.log(
      `${students[0].name} ${students[0].surname} is in "ვრაგ ნაროდ"!`
    );
claudeMonetArthAve > generalArthAve
  ? console.log(
      `${students[1].name} ${students[1].surname} got the Red Diploma!`
    )
  : console.log(
      `${students[1].name} ${students[1].surname} is in "ვრაგ ნაროდ"!`
    );
vanGoghArthAve > generalArthAve
  ? console.log(
      `${students[2].name} ${students[2].surname} got the Red Diploma!`
    )
  : console.log(
      `${students[2].name} ${students[2].surname} is in "ვრაგ ნაროდ"!`
    );
damSquareArthAve > generalArthAve
  ? console.log(
      `${students[3].name} ${students[3].surname} got the Red Diploma!`
    )
  : console.log(
      `${students[3].name} ${students[3].surname} is in "ვრაგ ნაროდ"!`
    );

// the best student with GPA
console.log("-------------------------------------------------------");
if (
  students[0].scores.GPA > students[1].scores.GPA &&
  students[0].scores.GPA > students[2].scores.GPA &&
  students[0].scores.GPA > students[3].scores.GPA
) {
  console.log(
    `${students[0].name} ${students[0].surname} has the highest GPA!`
  );
} else if (
  students[1].scores.GPA > students[0].scores.GPA &&
  students[1].scores.GPA > students[2].scores.GPA &&
  students[1].scores.GPA > students[3].scores.GPA
) {
  console.log(
    `${students[1].name} ${students[1].surname} has the highest GPA!`
  );
} else if (
  students[2].scores.GPA > students[0].scores.GPA &&
  students[2].scores.GPA > students[1].scores.GPA &&
  students[2].scores.GPA > students[3].scores.GPA
) {
  console.log(
    `${students[2].name} ${students[2].surname} has the highest GPA!`
  );
} else if (
  students[3].scores.GPA > students[0].scores.GPA &&
  students[3].scores.GPA > students[1].scores.GPA &&
  students[3].scores.GPA > students[2].scores.GPA
) {
  console.log(
    `${students[3].name} ${students[3].surname} has the highest GPA!`
  );
}

// the best student with the best arithmetical average (21+)
console.log("-------------------------------------------------------");
let bestArthAveOver21 = "";
let maxValue = 0;
let over21 = false;
if (students[0].age >= 21 && jeanRenoArthAve > maxValue) {
  maxValue = jeanRenoArthAve;
  bestArthAveOver21 = "Jean Reno";
  over21 = true;
}
if (students[1].age >= 21 && claudeMonetArthAve > maxValue) {
  maxValue = claudeMonetArthAve;
  bestArthAveOver21 = "Claude Monet";
  over21 = true;
}
if (students[2].age >= 21 && vanGoghArthAve > maxValue) {
  maxValue = vanGoghArthAve;
  bestArthAveOver21 = "Van Gogh";
  over21 = true;
}
if (students[3].age >= 21 && damSquareArthAve > maxValue) {
  maxValue = damSquareArthAve;
  bestArthAveOver21 = "Dam Square";
  over21 = true;
}
if (!over21) {
  bestArthAveOver21 = "No one";
}
console.log(`${bestArthAveOver21} has best arithmetical average over 21!`);

// the best front-end developer
console.log("-------------------------------------------------------");
let jeanRenoFrontEnd = students[0].scores.javascript + students[0].scores.react;
let claudeMonetFrontEnd =
  students[1].scores.javascript + students[1].scores.react;
let vanGoghFrontEnd = students[2].scores.javascript + students[2].scores.react;
let damSquareFrontEnd =
  students[3].scores.javascript + students[3].scores.react;
if (
  jeanRenoFrontEnd > claudeMonetFrontEnd &&
  jeanRenoFrontEnd > vanGoghFrontEnd &&
  jeanRenoFrontEnd > damSquareFrontEnd
) {
  console.log(
    `${students[0].name} ${students[0].surname} is the best front-end developer!`
  );
} else if (
  claudeMonetFrontEnd > jeanRenoFrontEnd &&
  claudeMonetFrontEnd > vanGoghFrontEnd &&
  claudeMonetFrontEnd > damSquareFrontEnd
) {
  console.log(
    `${students[1].name} ${students[1].surname} is the best front-end developer!`
  );
} else if (
  vanGoghFrontEnd > claudeMonetFrontEnd &&
  vanGoghFrontEnd > jeanRenoFrontEnd &&
  vanGoghFrontEnd > damSquareFrontEnd
) {
  console.log(
    `${students[2].name} ${students[2].surname} is the best front-end developer!`
  );
} else if (
  damSquareFrontEnd > claudeMonetFrontEnd &&
  damSquareFrontEnd > vanGoghFrontEnd &&
  damSquareFrontEnd > jeanRenoFrontEnd
) {
  console.log(
    `${students[3].name} ${students[3].surname} is the best front-end developer!`
  );
}
